extends TileMap

var gridsize:Vector2 = Vector2(64, 36) #Grid size
var adding:int = 0 #Is right click pressed?
var removing:int = 0 #Is left clicl pressed?

func _ready() -> void:
	pass

func _draw(): #Generate grid.
	for i in gridsize.x:
		draw_line(Vector2(i * 16, 0), Vector2(i * 16, 36 * 16), Color(0.1, 0.1, 0.1), 1.0)
	for i in gridsize.y:
		draw_line(Vector2(0, i * 16), Vector2(64 * 16, i * 16), Color(0.1, 0.1, 0.1), 1.0)

func _process(delta:float) -> void:
	if adding ^ removing: #If is adding or removing, but not both or neither
		var mvpos = world_to_map(get_global_mouse_position()) #Get mouse position to a Tile coordinates
		#The variable "adding" can be 0 or -1, like cells (-1 = empty = dead, 0 = not empty = alive), so I prefer to simplify it. :D
		if get_cellv(mvpos) == adding:
			#Same as above, I hope I have explained it well. :D
			set_cellv(mvpos, removing)

func _input(event) -> void:
	if event.is_action_pressed("ui_accept"): #To start the life.
		set_process(false) #Stop _proccess.
		$Timer.start() #And start the Timer.
	if event.is_action_pressed("add_cell"): #Left click pressed.
		adding = -1 #Start adding cells in _proccess.
	elif event.is_action_released("add_cell"): #Left click released.
		adding = 0 #Stop adding cells in proccess.
	elif event.is_action_pressed("remove_cell"): #Right click pressed.
		removing = -1 #Start removing cells in _proccess.
	elif event.is_action_released("remove_cell"): #Right click released.
		removing = 0 #Stop removing cells in _proccess.

func _change_cells() -> void: #The timer is connected to this function.
	var new_cells = [] #This will contain the next generation of cells.
	for i in gridsize.x: #Go through the whole grid in x axis.
		for j in gridsize.y: #Go through the whole grid in y axis.
			var neighbors = check_near(i, j) #Check if there are living cells around.
			if get_cell(i, j) == 0: #If the cell is alive...
				if neighbors == 2 or neighbors == 3: #...and has 2 or 3 cells around...
					new_cells.append(Vector2(i, j)) #...then add it to the array.
			else: #If the cell is dead...
				if neighbors == 3: #...and has 3 alive cells around...
					new_cells.append(Vector2(i, j)) #...then add it to the array.
	for i in get_used_cells(): #Get the cells alive...
		set_cellv(i, -1) #...and clean them.
	for i in new_cells: #Get the next generation...
		set_cellv(i, 0) #...and add it to the TileMap.

func check_near(xpos:int, ypos:int) -> int: #Check the living cells around.
	var neighbors = 0 #Number of cells around.
	for i in range(xpos - 1, xpos + 2): #Start from a cell before to a cell after in x axis...
		for j in range(ypos - 1, ypos + 2): #...and y axis too.
			if i == xpos and j == ypos: #If the cell is in the center, then we don't count it.
				continue
			if get_cell(i, j) == 0: #If the cell around is alive...
				neighbors += 1 #...then we count it.
	return neighbors #And return the number of living cells around. :D
