extends Node2D

onready var gridscn:PackedScene = preload("res://GridMap/GridMap.tscn")

func _ready() -> void:
	pass

func _input(event) -> void:
	if event.is_action_pressed("ui_accept"): #To start the game.
		if not has_node("GridMap"):
			if $Instructions.visible:
				$Instructions.hide()
				$Instructions2.show()
			elif $Instructions2.visible:
				$Instructions2.hide()
				var grid_node:TileMap = gridscn.instance()
				add_child(grid_node)
	elif event.is_action_pressed("ui_cancel"): #To exit to instructions.
		if has_node("GridMap"):
			get_node("GridMap").queue_free()
			$Instructions.show()
